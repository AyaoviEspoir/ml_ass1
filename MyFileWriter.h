#ifndef __MY_FILE_WRITER__
#define __MY_FILE_WRITER__

#include <iostream>
#include <string>
#include <utility>
#include <vector>
#include <sstream>
#include <fstream>

class MyFileWriter
{
	private:
		std::string _filename;
	
	public:
		/* Default constructor. */
		MyFileWriter();
		
		/* Alternative constructor. */
		MyFileWriter(std::string filename);
		
		/* Destructor. */
		~MyFileWriter();
		
		/* A function to write out data to a file. */
		void write(std::vector< std::vector< std::string> > & trainingData);
};

#endif

