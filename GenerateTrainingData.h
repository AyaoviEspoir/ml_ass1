#ifndef __GENERATE_TRAINING_DATA__
#define __GENERATE_TRAINING_DATA__

#include <iostream>
#include <vector>
#include <string>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>

class GenerateTrainingData
{
		private:
			std::vector< std::vector< std::string > > _fields;
			
			std::vector< std::string > _currentExample;
			
			int _lowerBound;
			
			int _upperBound;
			
		public:
			/* Default constructor. */
			GenerateTrainingData();
			
			/* Alternative constructor. */
			GenerateTrainingData(std::vector< std::vector< std::string >> & fields);
			
			/* Destruvtor */
			~GenerateTrainingData();
			
			/* Returns a generated data. */
			std::vector< std::string > generateExample();
};

#endif
