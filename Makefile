#================================================================
# Author: DJSAYA001.
# Date: 11/08/2015.
# Info: I have done my best to make this Makefile as generic as 
# 	I could so I beg you to bear with me.
#================================================================

CC=g++
CCFLAGS=-std=c++11
OBJECTS=FindS.o MyFileReader.o MyFileWriter.o GenerateTrainingData.o Driver.o
TEST=Utility.o Image.o unit_test.o
TARFILES= *.cpp *.h Makefile

# The following is just for getting the current path
mkfile_path := $(abspath $(lastword $(MAKEFILE_LIST)))
current_dir := $(notdir $(patsubst %/,%,$(dir $(mkfile_path))))

# Then I can easily get the path of the ReadMe.txt file
File = current_dir+/ReadMe.txt

# Here I would just be checking if the file specified by the path 
# above exist;

ifeq ($(wildcard $(File)),)
	TARFILES += ReadMe.txt
endif

findS: $(OBJECTS)
	$(CC) $(OBJECTS) -o $@ $(CCFLAGS)
	
#Pattern rule would compile every .cpp file into a .o file
%.o: %.cpp
	$(CC) $< -c $(CCFLAGS)
	
#These are for unit testing.
test: $(TEST)
	$(CC) $(TEST) -o $@ $(CCFLAGS)
	
#Here I have tar rule to make a tarball.
tar: clean $(TARFILES)
	mkdir submission
	cp $(TARFILES) submission
	cp -r ../resources/ submission
	cp -r .git/ submission
	tar cfv djsaya001.tar submission
	rm -rf submission

#Here I have tar rule to make a zip file.
zip: clean $(TARFILES)
	mkdir submission
	cp $(TARFILES) submission
	cp -r ../resources/ submission
	cp -r .git/ submission
	zip -r djsaya001.zip submission
	rm -rf submission
	
part1: clean
part1: CCFLAGS+=-DPART1
part1: findS

part2: clean
part2: CCFLAGS+=-DPART2
part2: findS

clean:
	rm -f *.o findS test *.tar *zip
	
uninstall:
	rm -f findS
