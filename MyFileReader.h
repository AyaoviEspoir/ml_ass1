#ifndef __MY_FILE_READER__
#define __MY_FILE_READER__

#include <iostream>
#include <string>
#include <vector>
#include <sstream>
#include <fstream>
#include <algorithm>

class MyFileReader
{
	private:
		std::string _filename;

	public:
		/* Default constructor. */
		MyFileReader();

		/* Alternative constructor. */
		MyFileReader(std::string filename);

		/* Destructor. */
		~MyFileReader();

		/* A function to read in data from a file. */
		void read(std::vector<std::vector<std::string>> & trainingData);

		/* A function to compute the size of the training example. */
		int getSize(std::string trainingExample);
};

#endif
