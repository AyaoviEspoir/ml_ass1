#include "FindS.h"
#include "MyFileReader.h"
#include <iostream>
#include <vector>
#include <string>

int main(int argc, char** argv)
{
	/*
	 * PART ONE.
	 * */
	
	#ifdef PART1
	
	MyFileReader * reader = new MyFileReader("../resources/trainingData.txt");

	std::vector<std::vector<std::string>> trainingData;

	reader->read(trainingData);

	FindS * findS = new FindS(trainingData);

	findS->iterate();		/* Aim is to find the hypothesis describing the trainingData.  */

	std::vector<std::string> hypothesis = findS->getHypothesis();

	std::cout << "============================================================" << std::endl;

	std::cout << "The concept learnt from the training data is : " << std::endl;

	findS->printConcept(hypothesis);

	std::cout << "============================================================" << std::endl;
	
	#endif
	
	/*
	 * PART TWO.
	 * */
	
	#ifdef PART2
	
	std::vector< std::string > targetConcept = {"Sunny", "Warm", "?", "?", "?", "?"};
	
	FindS * findS = new FindS(targetConcept);
	
	findS->generateTable();
	
	#endif
	
	return 0;
}
