================================================================
Author : djsaya001
Date: 14/08/2015
================================================================


The following instructions would help the user have an enjoyable experince using the findS program.

findS is made of 5 files - Driver class, FindS class, GenerateTrainingData class, MyFileReader class 
and MyFileWriter class - and a Makefile (here to make the user life easier). To make use of the program 
the user needs to first compile it and only then could it be executed.

1 - Compilation:

It is recommanded that the user works form a terminal (Ctrl+Alt+t on a Linux system) in th folder containing 
the files. The compilation process goes in two ways:
	
	-	First the user can opt to only see the first part of the program (where given a training data, the 
		program returns a concept learnt from the data) by giving the command 'make part1'.
		
	-	Second the user can opt to only see the second part of the program (where given a target concept, the 
		program returns a training data - random - required to learn the concept) by giving the command 'make part2'.

2 - Exection:

After compiling an executable file named findS should be created and the user just need to enter the command './findS' 
for execution. During execution, the program make use of files in the directory named 'resources/' as required.

Have fun using my simple program.
