#include "MyFileWriter.h"

MyFileWriter::MyFileWriter()
{}

MyFileWriter::MyFileWriter(std::string filename)
{
	_filename = filename;
}

MyFileWriter::~MyFileWriter() {}

void MyFileWriter::write(std::vector<std::vector< std::string> > & trainingData)
{
	std::ofstream myfile;
	
	myfile.open(_filename);
	
	if (!myfile) { return; }

	std::string example;
	
	for (int i = 0; i < trainingData.size(); ++i)
	{
		example = "";
		
		for (int j = 0; j < trainingData[i].size(); j++)
		{
			example += (trainingData[i][j] + " ");
		}

		myfile << example << std::endl;
	}

	myfile.close();
}
