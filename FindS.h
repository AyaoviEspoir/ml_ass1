#ifndef __FIND_S__
#define __FIND_S__

#include "GenerateTrainingData.h"
#include "MyFileWriter.h"
#include <iostream>
#include <vector>
#include <string>

class FindS
{
	private:
		std::vector< std::vector< std::string > > _trainingData;		/* As the name implies; the variable stores the training data. */

		std::vector< std::string > _concept;							/* The hypothesis as we iterate through. */
		
		std::vector< std::string > _currentTrainingExample;				/* The example as generated recently. */
		
		std::vector< std::string > _targetConcept;						/* Concept to be learnt. */
		
		GenerateTrainingData * _generator;								/* Provides with training examples. */
		
	public:
		/* 
		 * Default constructor. 
		 * */
		FindS();
		

		/* 
		 * Destructor. 
		 * */
		~FindS();

		#ifdef PART1

		/* 
		 * Second alternative constructor. 
		 * */
		FindS(std::vector<std::vector<std::string>> & trainingData);

		/* 
		 * A function that iterates through the training samples and generalises the hypothesis. 
		 * */
		void iterate();

		/* 
		 * Returns the hypothesis after iteration. 
		 * */
		std::vector<std::string> getHypothesis();		
		
		#endif

		#ifdef PART2

		/*
		 * Alternative constructor.
		 * */
		FindS(std::vector< std::string > & targetConcept);

		/*
		 * Checks whether a training example agrees with the target concept.
		 * */
		void validateCurrentTrainingExample();
	
		
		/*
		 * Generates and validate a training example.
		 * */
		void generateTrainingExample();
		
		/*
		 * 
		 * */
		void generateTable();
		
		/*
		 * Returns true is _concept is not same as _targetConpet.
		 * */
		bool conpectNotEqualTargetConcept();
				
		#endif

		/* 
		 * A function that initialize the hypotheosis to the specific. 
		 * */
		void initializeHypothesis();

		void printConcept(std::vector<std::string> & concept);
};

#endif
