#include "FindS.h"

FindS::FindS() {}

#ifdef PART2

FindS::FindS(std::vector< std::string > & targetConcept)
{
	_targetConcept = targetConcept;
	
	std::vector< std::vector< std::string >> fields = 	{{std::string("Clear"), std::string("Sunny"), std::string("Rainy")}, 
														{std::string("Warm"), std::string("Cold")}, 
														{std::string("Normal"), std::string("High"), std::string("Low")}, 
														{std::string("Strong"), std::string("Average"), std::string("Weak")}, 
														{std::string("Warm"), std::string("Cool"), std::string("Tepid")}, 
														{std::string("Same"), std::string("Change"), std::string("Slight")}};
	
	_generator = new GenerateTrainingData(fields);
	
	generateTrainingExample();
	
	initializeHypothesis();
}

#endif


#ifdef PART1

FindS::FindS(std::vector<std::vector<std::string>> & trainingData)
{
	_trainingData = trainingData;
	
	initializeHypothesis();
}

#endif

FindS::~FindS() {}

void FindS::initializeHypothesis()
{
	for (int i = 0; i < (_trainingData.front().size() - 1); i++)
	{
		_concept.push_back(std::string("O"));
	}
}

#ifdef PART1

std::vector<std::string> FindS::getHypothesis()
{
	return _concept;
}

void FindS::iterate()
{
	std::vector<std::vector<std::string>>::iterator outerIterator = _trainingData.begin();

	for (int j = 0; j < _trainingData.size(); j++)
	{
		if ((_trainingData[j].back().compare("Yes") == 0) || (_trainingData[j].back().compare("yes") == 0))
		{
			for (int i = 0; i < _concept.size(); i++)
			{		
				if (_concept[i].compare(_trainingData[j][i]) != 0)
				{
					if (_concept[i].compare("O") == 0)
					{
						_concept[i] = _trainingData[j][i];
					}

					else
					{
						_concept[i] = "?";
					}
				}
			}
		}

		else
		{
			continue;
		}
	}
}

#endif


#ifdef PART2

void FindS::generateTrainingExample()
{
	_currentTrainingExample = _generator->generateExample();
	
	validateCurrentTrainingExample();
	
	_trainingData.push_back(_currentTrainingExample);
}


void FindS::validateCurrentTrainingExample()
{
	std::string positivity = "Yes";
	
	for (int i = 0; i < _targetConcept.size(); i++)
	{
		if (_targetConcept[i].compare("?") == 0) { continue; }
		
		else
		{
			if (_currentTrainingExample[i].compare(_targetConcept[i]) != 0) 
			{
				positivity = "No";
				
				break;
			}
		}
	}
	
	_currentTrainingExample.push_back(positivity);
}

bool FindS::conpectNotEqualTargetConcept()
{
	bool result = true;
	
	for (int i = 0; i < _concept.size(); i++)
	{
		if (_concept[i].compare(_targetConcept[i]) != 0)
		{
			result = false;
			
			break;
		}
	}
	
	return !result;		/* !equal */
}

void FindS::generateTable()
{
	std::cout << "============================================================" << std::endl;
	
	std::cout << "Target concept is : " << std::endl;
	
	printConcept(_targetConcept);
	
	while(conpectNotEqualTargetConcept())
	{
		conpectNotEqualTargetConcept();
		
		generateTrainingExample();
		
		if ((_currentTrainingExample.back().compare("Yes") == 0) || (_currentTrainingExample.back().compare("yes") == 0))
		{
			for (int i = 0; i < _concept.size(); i++)
			{		
				if (_concept[i].compare(_currentTrainingExample[i]) != 0)
				{
					if (_concept[i].compare("O") == 0)
					{
						_concept[i] = _currentTrainingExample[i];
					}

					else
					{
						_concept[i] = "?";
					}
				}
			}
		}
	}
	
	std::cout << "Reached target concept..." << std::endl;
	
	std::cout << "Writing required training data to file..." << std::endl;
	
	/*
	 * Write data to file.
	 * */
	MyFileWriter * writer = new MyFileWriter("../resources/requiredTrainingData.txt");
	
	writer->write(_trainingData);
	
	std::cout << "Done" << std::endl;
	
	std::cout << "============================================================" << std::endl;
}

#endif

void FindS::printConcept(std::vector<std::string> & concept)
{
	std::cout << "< " << concept[0];

	for (int i = 1; i < concept.size(); i++)
	{
		std::cout << ", " << concept[i];
	}

	std::cout << " >" << std::endl;
}
