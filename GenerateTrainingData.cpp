#include "GenerateTrainingData.h"

GenerateTrainingData::GenerateTrainingData() {}

GenerateTrainingData::GenerateTrainingData(std::vector< std::vector< std::string >> & fields)
{
	_fields = fields;
	
	_lowerBound = 0;
}

GenerateTrainingData::~GenerateTrainingData() {}

std::vector< std::string > GenerateTrainingData::generateExample()
{
	int index;
	
	_currentExample.clear();
	
	for (int i = 0; i < _fields.size(); i++)
	{
		_upperBound = _fields[i].size() - 1;
	
		index = _lowerBound + (rand() % _upperBound);
		
		_currentExample.push_back(_fields[i][index]);
	}
	
	return _currentExample;
}
