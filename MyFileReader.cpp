#include "MyFileReader.h"

MyFileReader::MyFileReader()
{}

MyFileReader::MyFileReader(std::string filename)
{
	_filename = filename;
}

MyFileReader::~MyFileReader() {}

int MyFileReader::getSize(std::string trainingExample)
{
	return (int) (std::count(trainingExample.begin(), trainingExample.end(), ' ') + 1);
}

void MyFileReader::read(std::vector<std::vector<std::string>> & trainingData)
{
	std::ifstream myfile;

	myfile.open(_filename);

	if (!myfile) { return; }

	std::string data;

	while (std::getline(myfile, data))
	{
		std::istringstream iss(data);

		std::vector<std::string> uniqueExample;

		std::string element;

		for (int i = 0; i < getSize(data); i++)
		{
			iss >> element;

			uniqueExample.push_back(element);
		}

		trainingData.push_back(uniqueExample);
	}

	myfile.close();
}
